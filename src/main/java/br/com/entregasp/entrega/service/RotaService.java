package br.com.entregasp.entrega.service;

import java.util.List;

import br.com.entregasp.entrega.entities.Mapa;

public interface RotaService {

	List<Mapa> calcularRota(Mapa mapa);

}
