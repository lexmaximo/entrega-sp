package br.com.entregasp.entrega.dao;

import java.util.List;

import br.com.entregasp.entrega.entities.Mapa;

public interface MapaDao {

	void save(Mapa mapa);

	void atualizar(Mapa mapa);

	void excluir(Mapa mapa);

	Mapa findMapa(Mapa mapa);

	Mapa findMapaById(Integer id);

	List<Mapa> findAllMapas();

}
