package br.com.entregasp.entrega.dao;

import java.util.List;

import br.com.entregasp.entrega.entities.Mapa;

public interface RotaDao {

	List<Mapa> calcularRota(Mapa mapa);

}
